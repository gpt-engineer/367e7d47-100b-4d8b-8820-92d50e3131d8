// Array of rainbow colors
const rainbowColors = ["text-red-500", "text-orange-500", "text-yellow-500", "text-green-500", "text-blue-500", "text-indigo-500", "text-violet-500"];
let currentColorIndex = 0;

document.addEventListener("DOMContentLoaded", () => {
  const addButton = document.getElementById("add-todo");
  const inputField = document.getElementById("todo-input");
  const todoList = document.getElementById("todo-list");

  addButton.addEventListener("click", () => {
    const todoText = inputField.value.trim();
    if (todoText) {
      const listItem = document.createElement("li");
      // Get the next rainbow color
      const colorClass = rainbowColors[currentColorIndex];
      // Update the current color index
      currentColorIndex = (currentColorIndex + 1) % rainbowColors.length;
      listItem.innerHTML = `
        <span class="flex-1 ${colorClass}">${todoText}</span>
        <button class="complete-todo"><i class="fas fa-check"></i></button>
        <button class="delete-todo"><i class="fas fa-trash"></i></button>
      `;
      listItem.classList.add("flex", "mb-2", "items-center");
      todoList.appendChild(listItem);

      // Clear the input field
      inputField.value = "";

      // Add event listeners for the complete and delete buttons
      listItem.querySelector(".complete-todo").addEventListener("click", () => {
        listItem.querySelector("span").classList.toggle("line-through");
      });

      listItem.querySelector(".delete-todo").addEventListener("click", () => {
        todoList.removeChild(listItem);
      });
    }
  });
});
